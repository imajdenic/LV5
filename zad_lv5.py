# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 10:26:07 2015

@author: ilija
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.preprocessing import PolynomialFeatures


def generate_data(n):

 #prva klasa
 n1 = n/2
 x1_1 = np.random.normal(0.0, 2, (n1,1));
 #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
 x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
 y_1 = np.zeros([n1,1])
 temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)

 #druga klasa
 n2 = n - n/2
 x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
 y_2 = np.ones([n2,1])
 temp2 = np.concatenate((x_2,y_2),axis = 1)

 data = np.concatenate((temp1,temp2),axis = 0)

 #permutiraj podatke
 indices = np.random.permutation(n)
 data = data[indices,:]

 return data
 
 
##2.zad 
np.random.seed(242)
xtrain=generate_data(200)
np.random.seed(12)
xtest=generate_data(100)
plt.scatter(xtrain[:,0],xtrain[:,1],c=xtrain[:,2])


##3.zad
logisticModel = lm.LogisticRegression()
logisticModel.fit(xtrain[:,0:2],xtrain[:,2])
theta0=logisticModel.intercept_
theta1=logisticModel.coef_[0,0]
theta2=logisticModel.coef_[0,1]
x1=-6
x2=(-theta0-theta1*x1)/theta2
print x2
x11=6
x21=(-theta0-theta1*x11)/theta2
print x21
plt.plot(np.array([-6,6]),np.array([x2,x21]))


##4.zad
f, ax = plt.subplots(figsize=(8, 6))
x_grid, y_grid = np.mgrid[min(xtrain[:,0])-0.5:max(xtrain[:,0])+0.5:.05,
                          min(xtrain[:,1])-0.5:max(xtrain[:,1])+0.5:.05]
grid = np.c_[x_grid.ravel(), y_grid.ravel()]
probs = logisticModel.predict_proba(grid)[:, 1].reshape(x_grid.shape)

cont = ax.contourf(x_grid, y_grid, probs, 60, cmap="Greys", vmin=0, vmax=1)
ax_c = f.colorbar(cont)
ax_c.set_label("$P(y = 1|\mathbf{x})$")
ax_c.set_ticks([0, .25, .5, .75, 1])
ax.set_xlabel('$x_1$', alpha=0.9)
ax.set_ylabel('$x_2$', alpha=0.9)
ax.set_title('Izlaz logisticke regresije')
plt.scatter(xtrain[:,0],xtrain[:,1],c=xtrain[:,2])
plt.show()


##5.zad
x_testP =  logisticModel.predict(xtest[:,0:2])
greska=np.subtract(x_testP,xtest[:,2])    
fig, ax = plt.subplots()
im = ax.scatter(xtest[:,0], xtest[:,1], c=greska)


##6.zad
def plot_confusion_matrix(c_matrix):
 norm_conf = []
 for i in c_matrix:
   a = 0
   tmp_arr = []
   a = sum(i, 0)
   for j in i:
       tmp_arr.append(float(j)/float(a))
   norm_conf.append(tmp_arr)
 fig = plt.figure()
 ax = fig.add_subplot(111)
 res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
 width = len(c_matrix)
 height = len(c_matrix[0])
 for x in xrange(width):
   for y in xrange(height):
     ax.annotate(str(c_matrix[x][y]), xy=(y, x),
            horizontalalignment='center',
            verticalalignment='center', color = 'green', size = 20)
 fig.colorbar(res)
 numbers = '0123456789'
 plt.xticks(range(width), numbers[:width])
 plt.yticks(range(height), numbers[:height])
 plt.ylabel('Stvarna klasa')
 plt.title('Predvideno modelom')
 plt.show()
zbunjujucaMatrica=confusion_matrix(xtest[:,2],x_testP)
plt.figure()
plot_confusion_matrix(zbunjujucaMatrica)
accuracy=accuracy_score(xtest[:,2], x_testP)
print 'accuracy:',accuracy
missclacification_rate=1-accuracy
print 'missclacification_rate:',missclacification_rate
precision_score=precision_score(xtest[:,2], x_testP)  
print 'precision_score:',precision_score
recall_score=recall_score(xtest[:,2], x_testP) 
print 'recall_score:',recall_score


##7.zad
#3 zad ponovljen
poly = PolynomialFeatures(degree=3, include_bias = False)
data_train_new = poly.fit_transform(xtrain[:,0:2])
plt.scatter(data_train_new[:,0],data_train_new[:,1],c=data_train_new[:,2])
logisticModel = lm.LogisticRegression()
logisticModel.fit(data_train_new,xtrain[:,2])
theta0_0=logisticModel.intercept_
theta1_1=logisticModel.coef_[0,0]
theta2_2=logisticModel.coef_[0,1]
x1_1=-6
x2_1=(-theta0-theta1*x1_1)/theta2
print x2_1
x11_1=6
x21_1=(-theta0-theta1*x11_1)/theta2
print x21_1
plt.plot(np.array([-6,6]),np.array([x2_1,x21_1]))



